package com.example.airliftcase.main

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.example.airliftcase.BuildConfig
import com.example.airliftcase.common.database.TestDatabase
import com.example.airliftcase.common.network.NetworkApiClient
import okhttp3.HttpUrl.Companion.toHttpUrl
import java.util.concurrent.TimeUnit

class CoreApplication : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        var database: TestDatabase? = null

        @JvmStatic
        fun getAppContext(): Context {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        initializeNetworkApiClient()
        initializeDatabase()
    }


    private fun initializeDatabase() {
        database = TestDatabase.getInstance(context)
    }

    private fun initializeNetworkApiClient() {

        val apiClientConfiguration =
            NetworkApiClient.ApiClientConfiguration(BuildConfig.API_SERVER.toHttpUrl()).apply {
                addTimeOutInMinutes(2, TimeUnit.MINUTES)
            }

        NetworkApiClient.initializeNetworkClient(apiClientConfiguration)
    }
}
