package com.example.airliftcase.main

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.airliftcase.R
import com.example.airliftcase.common.util.fragments.FragmentUtils
import com.example.airliftcase.common.util.glide.GlideUtils
import com.example.airliftcase.common.view.BaseActivity
import com.example.airliftcase.databinding.ActivityMainBinding
import com.example.airliftcase.ui.cart.CartFragment
import com.example.airliftcase.ui.home.HomeFragment
import com.example.airliftcase.ui.search.SearchFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class MainActivity : BaseActivity(), NavigationBarView.OnItemSelectedListener {

    private val getProductSubject = PublishSubject.create<Boolean>()
    private lateinit var binding: ActivityMainBinding
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var fragmentUtils: FragmentUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        init()
        listeners()
    }

    private fun init() {
        fragmentUtils = FragmentUtils(this)
        bottomNavigation = binding.navigationView
        fragmentUtils.openfragment(HomeFragment(), R.id.container)
    }

    private fun listeners() {
        bottomNavigation.setOnItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> {
                fragmentUtils.openfragment(HomeFragment(), R.id.container)
                return true
            }
            R.id.navigation_cart -> {
                fragmentUtils.openfragment(CartFragment(), R.id.container)
                return true
            }
            R.id.navigation_search -> {
                fragmentUtils.openfragment(SearchFragment(), R.id.container)
                return true
            }
            else -> return false

        }

    }

}

@BindingAdapter("imageUrl")
fun setImageUrl(imgView: AppCompatImageView, imageUrl: String) {
    imgView.setImageBitmap(null)

    if (imageUrl.isNotEmpty()) {
        GlideUtils.loadImage(imageUrl, imgView)
    }
}

@BindingAdapter("imageDrawable")
fun setImageDrawable(imageView: AppCompatImageView, imageDrawable: Int) {
    imageView.setImageBitmap(null)
    if (imageDrawable != 0){
        GlideUtils.loadImageDrawable(imageView,imageDrawable)
    }
}