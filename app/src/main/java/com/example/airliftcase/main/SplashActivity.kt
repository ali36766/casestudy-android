package com.example.airliftcase.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.example.airliftcase.R
import com.example.airliftcase.common.view.BaseActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val runnable = Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        executeRunnable(runnable)
    }

    //Usually Not Used in Real Scenarios as Necessary API are called and Main Activity is launched when a valid response is received.
    private fun executeRunnable(runnable: Runnable) {
        val handler = Handler()
        handler.postDelayed(runnable, 2000)
    }
}