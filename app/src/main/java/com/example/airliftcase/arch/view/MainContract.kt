package com.example.airliftcase.arch.view

import com.example.airliftcase.common.base.*
import io.reactivex.Observable

interface MainContract {
    interface View : BaseMvpView {
        fun getProductsIntent(): Observable<Boolean>
        fun getCategoriesIntent(): Observable<Boolean>
    }

    abstract class Presenter : BasePresenter<View, BaseViewState>()

    abstract class GetProductListInteractor : BaseInteractor() {

        abstract fun proceed(): Observable<BasePartialChanges>
    }

    abstract class GetCategoriesInteractor : BaseInteractor() {
        abstract fun proceed(): Observable<BasePartialChanges>
    }
}