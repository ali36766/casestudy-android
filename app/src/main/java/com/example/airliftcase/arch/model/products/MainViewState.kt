package com.example.airliftcase.arch.model.products

import android.os.Parcelable
import com.example.airliftcase.common.base.BaseViewState
import com.example.airliftcase.common.models.CategoryDetail
import com.example.airliftcase.common.models.ProductDetail
import kotlinx.parcelize.Parcelize

class MainViewState(
    var productList: MutableList<ProductDetail> = mutableListOf(),
    var categoryList: MutableList<CategoryDetail> = mutableListOf()
) : BaseViewState() {
}

