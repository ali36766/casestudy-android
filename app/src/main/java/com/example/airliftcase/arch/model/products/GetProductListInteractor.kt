package com.example.airliftcase.arch.model.products

import com.example.airliftcase.arch.intent.products.MainPartialChanges
import com.example.airliftcase.arch.view.MainContract
import com.example.airliftcase.common.base.BasePartialChanges
import com.example.airliftcase.common.models.ProductDetail
import com.example.airliftcase.common.network.NetworkApiClient
import com.example.airliftcase.common.network.api.ProductListApi
import com.example.airliftcase.common.util.constants.Constants
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import org.json.JSONArray

class GetProductListInteractor : MainContract.GetProductListInteractor() {
    override fun proceed(): Observable<BasePartialChanges> {
        val productApi = NetworkApiClient
            .getApi(ProductListApi::class.java)
            .getProducts()

        return doProcessRemoteApiRequest(productApi)
    }

    override fun parseResponse(success: String): BasePartialChanges {
        val type = object : TypeToken<List<ProductDetail>>() {}.type
        val products = GsonBuilder().create()
            .fromJson<ArrayList<ProductDetail>>(JSONArray(success).toString(), type)
        return MainPartialChanges.ProductListLoaded(
            Constants.PRODUCTS_RETRIEVED,
            products
        )
    }

}