package com.example.airliftcase.arch.intent.products

import com.example.airliftcase.arch.model.products.MainViewState
import com.example.airliftcase.common.base.BasePartialChanges
import com.example.airliftcase.common.base.BaseViewState
import com.example.airliftcase.common.models.CategoryDetail
import com.example.airliftcase.common.models.ProductDetail

sealed class MainPartialChanges : BasePartialChanges() {
    data class ProductListLoaded(val code: Int, val productList: MutableList<ProductDetail>) :
        MainPartialChanges() {
        override fun reduce(state: BaseViewState): BaseViewState {
            state as MainViewState
            state.productList = productList
            state.responseCode = code
            state.loading = false
            state.showError = false
            return state
        }

    }

    data class CategoriesListLoaded(val code: Int, val categoryList: MutableList<CategoryDetail>) :
        MainPartialChanges() {
        override fun reduce(state: BaseViewState): BaseViewState {
            state as MainViewState
            state.categoryList = categoryList
            state.responseCode = code
            state.loading = false
            state.showError = false
            return state
        }

    }

}