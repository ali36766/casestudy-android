package com.example.airliftcase.arch.view

import com.example.airliftcase.arch.model.products.GetCategoriesInteractor
import com.example.airliftcase.arch.model.products.GetProductListInteractor
import com.example.airliftcase.arch.model.products.MainViewState
import io.reactivex.Observable

class MainPresenter(
    val getProductListInteractor: MainContract.GetProductListInteractor = GetProductListInteractor(),
    val getCategoriesInteractor: MainContract.GetCategoriesInteractor = GetCategoriesInteractor()
) : MainContract.Presenter() {
    override fun bindIntents() {
        val getProductsIntent = intent { it.getProductsIntent() }
            .switchMap { getProductListInteractor.proceed() }

        val getCategoriesIntent = intent { it.getCategoriesIntent() }
            .switchMap { (getCategoriesInteractor.proceed()) }

        val mergedIntents = Observable.merge(getProductsIntent, getCategoriesIntent)
        subscribeViewState(
            mergedIntents.scan(MainViewState(), this::viewStateReducer),
            MainContract.View::render
        )


    }
}