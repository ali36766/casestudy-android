package com.example.airliftcase.arch.model.products

import com.example.airliftcase.R
import com.example.airliftcase.arch.intent.products.MainPartialChanges
import com.example.airliftcase.arch.view.MainContract
import com.example.airliftcase.common.base.BasePartialChanges
import com.example.airliftcase.common.models.CategoryDetail
import com.example.airliftcase.common.models.ProductDetail
import com.example.airliftcase.common.network.NetworkApiClient
import com.example.airliftcase.common.network.api.ProductListApi
import com.example.airliftcase.common.util.constants.Constants
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import java.io.IOException

class GetCategoriesInteractor : MainContract.GetCategoriesInteractor() {
    override fun proceed(): Observable<BasePartialChanges> {
        return Observable.fromCallable {
            MainPartialChanges.CategoriesListLoaded(
                Constants.CATEGORIES_RETRIEVED,
                dummyCategories()
            )
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .cast(BasePartialChanges::class.java)
            .onErrorReturn { throwable ->
                if (throwable is IOException)
                    getErrorResponse(code = Constants.INTERNET_CONNECTION_ERROR, throwable.message)
                else
                    getErrorResponse(Constants.DEFAULT_ERROR_CODE, throwable.message)
            }
            .startWith(BasePartialChanges.Loading)
    }

    //API Could have been used here , but Categories END POINT of my Chosen Provider didn't returned categories in JSON, hence for the time being Dummy Data is used
    private fun dummyCategories(): MutableList<CategoryDetail> {
        val categories = mutableListOf<CategoryDetail>()
        categories.add(CategoryDetail("Electronics", R.drawable.electronics))
        categories.add(CategoryDetail("Jewelry", R.drawable.earrings))
        categories.add(CategoryDetail("Mens Clothing", R.drawable.clothing))
        categories.add(CategoryDetail("Women Clothing", R.drawable.dress))
        return categories
    }
}