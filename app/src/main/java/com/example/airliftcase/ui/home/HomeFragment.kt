package com.example.airliftcase.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.airliftcase.R
import com.example.airliftcase.arch.model.products.MainViewState
import com.example.airliftcase.arch.view.MainContract
import com.example.airliftcase.arch.view.MainPresenter
import com.example.airliftcase.common.adapters.ItemLayoutManger
import com.example.airliftcase.common.adapters.ListingAdaptor
import com.example.airliftcase.common.base.BaseViewState
import com.example.airliftcase.common.models.CategoryDetail
import com.example.airliftcase.common.models.ProductDetail
import com.example.airliftcase.common.util.constants.Constants
import com.example.airliftcase.databinding.FragmentHomeBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class HomeFragment : Fragment(), MainContract.View {
    private lateinit var binding: FragmentHomeBinding
    private var productsList = emptyList<ProductDetail>()
    private var categoryList = emptyList<CategoryDetail>()

    private var getProductSubject = PublishSubject.create<Boolean>()
    private var getCategorySubject = PublishSubject.create<Boolean>()
    private val presenter: MainContract.Presenter by lazy {
        MainPresenter()
    }
    private val categories: ListingAdaptor<CategoryDetail> by lazy {
        ListingAdaptor<CategoryDetail>(requireContext(), categoryLayoutManager)
    }

    private val products: ListingAdaptor<ProductDetail> by lazy {
        ListingAdaptor<ProductDetail>(requireContext(), productLayoutManger)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.actionBar?.hide()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        init()
    }

    private fun init() {
        val productsLayoutManager =
            GridLayoutManager(requireContext(), 2, RecyclerView.HORIZONTAL, false)
        val categoryLayoutManager =
            GridLayoutManager(requireContext(), 1, RecyclerView.HORIZONTAL, false)
        products.bindRecyclerView(binding.rvProducts, productsLayoutManager)
        categories.bindRecyclerView(binding.rvCategories, categoryLayoutManager)
        getProductSubject.onNext(true)
        getCategorySubject.onNext(true)

    }


    private val categoryLayoutManager = object : ItemLayoutManger {
        override fun getLayoutId(position: Int): Int {
            return R.layout.layout_category_card
        }
    }

    private val productLayoutManger = object : ItemLayoutManger {
        override fun getLayoutId(position: Int): Int {
            return R.layout.layout_product_card
        }

        override fun bindView(view: View, getAdapterPosition: () -> Int) {
            super.bindView(view, getAdapterPosition)
            view.setOnClickListener {
                //Launch Pop-up window with Product Details e.g.
                //popupClass.launch(products.getItem(getAdapterPostion())
            }
        }
    }


    override fun getProductsIntent(): Observable<Boolean> {
        return getProductSubject
    }

    override fun getCategoriesIntent(): Observable<Boolean> {
        return getCategorySubject
    }

    override fun render(viewState: BaseViewState) {
        if (viewState.responseCode == Constants.PRODUCTS_RETRIEVED) {
            productsList = (viewState as MainViewState).productList
            products.addItems(productsList as ArrayList<ProductDetail>)
        }
        if (viewState.responseCode == Constants.CATEGORIES_RETRIEVED) {
            categoryList = (viewState as MainViewState).categoryList
            categories.addItems(categoryList as ArrayList<CategoryDetail>)
        }
        if (viewState.responseCode == Constants.INTERNET_CONNECTION_ERROR) {

            Toast.makeText(requireContext(), "Check your Internet Connection", Toast.LENGTH_SHORT)
                .show()
        }
    }

}
