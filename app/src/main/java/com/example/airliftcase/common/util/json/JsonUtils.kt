package com.example.airliftcase.common.util.json

import org.json.JSONArray
import org.json.JSONObject

object JsonUtils {

    fun createJSONObject(input: String): JSONObject {
        return JSONObject(input)
    }

    fun getJSONObject(jsonString: String, key: String): JSONObject {
        return createJSONObject(jsonString).getJSONObject(key)
    }

    @JvmStatic
    fun validateJSONString(string: String?): Boolean {
        return try {
            if (string != null) {
                JSONObject(string)
                true
            } else
                false
        } catch (e: Exception) {
            try {
                JSONArray(string)
                true
            } catch (e: Exception) {
                false
            }

        }
    }
}