package com.example.airliftcase.common.base

import com.example.airliftcase.common.util.constants.Constants

open class BaseViewState {

    var loading: Boolean = false
    var showError: Boolean = false
    var errorMessage = ""
    var responseCode: Int = Constants.DEFAULT_ERROR_CODE

    fun success(
        loading: Boolean = false,
        showError: Boolean = false,
        responseCode: Int
    ): BaseViewState {
        this.loading = loading
        this.showError = showError
        this.responseCode = responseCode
        return this
    }

    fun error(
        loading: Boolean = false,
        showError: Boolean = true,
        responseCode: Int,
        errorMessage: String
    ): BaseViewState {
        this.loading = loading
        this.showError = showError
        this.responseCode = responseCode
        this.errorMessage = errorMessage
        return this
    }
}