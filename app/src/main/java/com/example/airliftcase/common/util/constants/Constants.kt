package com.example.airliftcase.common.util.constants

object Constants {

    //ERROR CODES
    const val DEFAULT_ERROR_CODE = 1
    const val INTERNET_CONNECTION_ERROR = 2

    //ERROR MESSAGES
    const val GENERIC_ERROR_MESSAGE = "We are unable to process your request. Please try again."

    //SUCCESS CODE
    const val RESPONSE_OK = 200
    const val PRODUCTS_RETRIEVED = 201
    const val CATEGORIES_RETRIEVED = 202

}