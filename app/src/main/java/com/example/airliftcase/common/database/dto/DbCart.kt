package com.example.airliftcase.common.database.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserCart")
class DbCart(
    @ColumnInfo(index = true)
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val productName: String,
    val productPrice: String,
    val productDescription: String,
    val productCategory: String,
    val productImage: String? = null
) {

}