package com.example.airliftcase.common.base

interface PartialChangesCallback {

    fun reduce(state: BaseViewState): BaseViewState
}