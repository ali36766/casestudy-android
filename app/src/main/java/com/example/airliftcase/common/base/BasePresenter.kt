package com.example.airliftcase.common.base

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter

abstract class BasePresenter<V : BaseMvpView, S : BaseViewState> : MviBasePresenter<V, S>() {
    fun viewStateReducer(
        previousState: BaseViewState,
        partialChanges: BasePartialChanges
    ) = partialChanges.reduce(previousState)
}