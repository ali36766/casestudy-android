package com.example.airliftcase.common.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProductDetail(

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("price")
    var price: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("category")
    var category: String? = null,

    @SerializedName("image")
    var image: String? = null

) : Serializable {

}