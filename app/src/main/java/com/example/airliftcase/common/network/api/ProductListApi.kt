package com.example.airliftcase.common.network.api

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET

interface ProductListApi {

    @GET("products")
    fun getProducts(): Observable<Response<ResponseBody>>

    @GET("products/categories")
    fun getCategories(): Observable<Response<ResponseBody>>
}