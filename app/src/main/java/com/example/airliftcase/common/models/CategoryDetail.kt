package com.example.airliftcase.common.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CategoryDetail(
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("image")
    var image:Int = 0
) : Serializable {
}