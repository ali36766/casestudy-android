package com.example.airliftcase.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.airliftcase.common.database.dao.CartDao
import com.example.airliftcase.common.database.dto.DbCart

@Database(
    version = 1, entities = [
        DbCart::class
    ]
)
abstract class TestDatabase : RoomDatabase() {

    abstract fun getDao(): CartDao

    companion object {
        private val sLock = Any()
        private var INSTANCE: TestDatabase? = null

        @JvmStatic

        fun getInstance(context: Context): TestDatabase {
            synchronized(sLock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context, TestDatabase::class.java, "db_test")
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}