package com.example.airliftcase.common.base

import com.hannesdorfmann.mosby3.mvp.MvpView

interface BaseMvpView : MvpView {
    fun render(viewState: BaseViewState)
}