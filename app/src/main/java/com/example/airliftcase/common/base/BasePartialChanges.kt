package com.example.airliftcase.common.base

import com.example.airliftcase.common.util.constants.Constants

abstract class BasePartialChanges : PartialChangesCallback {

    object Loading : BasePartialChanges() {

        override fun reduce(state: BaseViewState): BaseViewState {
            state.loading = true
            state.showError = false
            state.errorMessage = ""
            state.responseCode = Constants.DEFAULT_ERROR_CODE
            return state
        }
    }

    class Error(val message: String = "", var code: Int = 0) : BasePartialChanges() {

        override fun reduce(state: BaseViewState): BaseViewState {
            return state.error(errorMessage = message, responseCode = code)
        }
    }

    data class Success(val code: Int) : BasePartialChanges() {

        override fun reduce(state: BaseViewState): BaseViewState {
            return state.success(responseCode = code)
        }
    }
}