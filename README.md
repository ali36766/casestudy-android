# Sample ECommerce Case Study:
This Application retrieves list of Products from a Dummy API and displays it to the user.

# Architecture:
This project utilized MVI Architecure that has been used in conjuction with RX Java and several common Design Patterns

MVI was designed around the paradigm of reactive programming and uses observable flows to exchange messages between different entities. As a result, each of them will be independent and therefore more flexible and resilient.
In addition, information will always flow in one direction: this concept is known as Unidirectional Data Flow or UDF. Once the architecture is established, the developer will find it easier to reason and debug if necessary.

# Architecture Components:
MVI Architecture is built upon the following Components 

1. BaseView State 
2. BasePartial Changes 
3. Presenter
4. Contract
5. Interactor 

# Patterns Used:

This Project has utilized the following patterns

1. Observable & Observer Pattern
2. Publish & Subscribe Pattern
3. Builder Pattern
4. Singleton Pattern

# How to Run:

This project has been fully configured to run seamlessly in Android Studio, However following things are mandatory

1. Android Studio Version should be Arctic Fox 2020.3.1 or Above 
2. Use Internal Embedded JDK if prompted by Android Studio 

To Run , Simply take pull from main branch and import project in android studio and choose build variant debug.

# Challenges: 

No Major Challenge was faced during the development of this project 

